# vector-2d


## Records


### vector-2d

Fields:
- width (number)
- height (number)


## Procedures


### vector-2d?

Arguments:
- object (object)

Returns
- (boolean) Returns #t if object is vector-2d


### vector-2d-make

Arguments:
- width (number)
- height (number)
- fill (object) Fill the 2d vector with this value


### vector-2d-get

Arguments:
- vector-2d (vector-2d)
- x (number) The x position of the cell
- y (number) The y position of the cell

Returns
- (object) The contents of cell in position x y


### vector-2d-set!

Arguments:
- vector-2d (vector-2d)
- x (number) The x position of the cell
- y (number) The y position of the cell
- value (object) The new value of the cell


### vector-2d-display

Arguments:
- vector-2d (vector-2d)


