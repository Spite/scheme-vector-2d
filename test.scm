(import (scheme base)
        (scheme write)
        (retropikzel 2d-vector v0.1.0 main))


(define test (2d-vector-make 10 10 0))
(2d-vector-display test)
(newline)

(2d-vector-set! test 5 5 1)
(2d-vector-display test)
(newline)
(2d-vector-set! test 1 5 1)
(2d-vector-display test)
(newline)
